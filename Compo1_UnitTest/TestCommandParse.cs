﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Compo_1_GraphicalProgrammingLanguage;
using System.Drawing;
using System.Windows.Forms;
using System;

namespace Compo1_UnitTest
{
    [TestClass]
    public class TestCommandParse
    {
        [TestMethod]
        public void TestDwarwLineWithWrongParameter()
        {
            PictureBox pic = new PictureBox();
            CommandParse cp = new CommandParse(new Create(Graphics.FromImage(new Bitmap(400, 300)), pic));

            try
            {
                cp.DrawFromLine("rect x,y", 0);

            }catch(InvalidParameter ex)
            {
                StringAssert.Contains(ex.Message, "Wrong Parameter given");
                return;
            }
            Assert.Fail("Expected exception was not thrown.");

        }

        [TestMethod]
        public void TestDrawLineWithWrongCommand()
        {
            PictureBox pic = new PictureBox();
            
            CommandParse cp = new CommandParse(new Create(Graphics.FromImage(new Bitmap(400, 300)), pic));

            try
            {
                cp.DrawFromLine("test", 0);

            }catch(InvalidCommand ex)
            {
                Console.WriteLine(ex);
                StringAssert.Contains(ex.Message, "Wrong command entered");
                return;
            }
            Assert.Fail("Exception was not thrown.");

        }

        [TestMethod]
        public void TestDwarwLineWithRightParameter()
        {
            PictureBox pic = new PictureBox();
            CommandParse cp = new CommandParse(new Create(Graphics.FromImage(new Bitmap(400, 300)), pic));
            bool check = false;

            try
            {
                cp.DrawFromLine("rect 100,100", 0);
                check= true;

            }
            catch (InvalidParameter ex)
            {
                StringAssert.Contains(ex.Message, "Wrong Parameter given");
                return;
            }
            Assert.IsTrue(check,"Expected exception run successfully.");

        }

    

    }
}
