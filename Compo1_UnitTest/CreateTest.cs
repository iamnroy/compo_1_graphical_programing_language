﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Compo_1_GraphicalProgrammingLanguage;
using System.Drawing;
using System.Windows.Forms;

namespace Compo1_UnitTest
{
    [TestClass]
    public class CreateTest
    {
        [TestMethod]
        public void MoveToTest()
        {
            System.Windows.Forms.PictureBox pic = new System.Windows.Forms.PictureBox();
            Create cre;
            CommandParse cp;
            cre = new Create(Graphics.FromImage(new Bitmap(400, 300)), pic);
            cp = new CommandParse(cre);
            cp.DrawFromLine("moveto 100,100", 0);
            int xPos = cre.xPos;

            Assert.AreEqual(100, xPos, 0.001, "xPos value dis not change after");
        }
    }
}
