﻿using System;
using System.Collections;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public class Compile
    {
        public ArrayList errors;


        public Compile()
        {
            errors = new ArrayList();
        }


        public ArrayList getErrors()
        {
            return errors;
        }

        public int getErrorCount()
        {
            return errors.Count;
        }
        public void ClearErrors()
        {
            errors.Clear();
        }
    }
}
