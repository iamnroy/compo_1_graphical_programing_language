﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// creating class for the invalid assignment value.
    /// </summary>
    class InvalidValue : Exception
    {

        public InvalidValue() : base("Invalid command given.") { }

        public InvalidValue(string message) : base(message) { }

        public InvalidValue(string message, System.Exception inner) : base(message, inner) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected InvalidValue(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
