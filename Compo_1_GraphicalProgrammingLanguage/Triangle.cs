﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// Creating triangle clas
    /// </summary>
    class Triangle : Shape
    {
        int width, height; //variable of width and triangle
        public Triangle() : base()
        {
            Console.WriteLine("rect obj created");
            width = 100;
            height = 100;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="colour"> Color of triangle</param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width">width of triangle</param>
        /// <param name="height"> height of triangle</param>
        public Triangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        public override void set(Color colour, params int[] list)
        {

            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"> object of graphics to create object</param>
        /// <param name="fill"> for filling the shape</param>
        override
        public void draw(Graphics g, bool fill)
        {
            Pen p = new Pen(base.colour, 2);
            SolidBrush b = new SolidBrush(base.colour);
            int xPos = x - width / 2;
            int yPos = y + height / 2;

            if (fill)
                g.FillPolygon(b, new Point[] { new Point(xPos, yPos), new Point(x + width / 2, yPos), new Point(x, y - height / 2) });
            else
                g.DrawPolygon(p, new Point[] { new Point(xPos, yPos), new Point(x + width / 2, yPos), new Point(x, y - height / 2) });

            Console.WriteLine("this is the base colour" + base.colour);
        }

        override
        public double calcArea()
        {
            return width * height;
        }

        override
        public double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }

}
