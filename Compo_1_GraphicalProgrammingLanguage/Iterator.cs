﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public class Iterator
    {
        private MethodCollection select;
        private int use = 0;
        private int move = 1;

        public Iterator(MethodCollection selection)
        {
            this.select = selection;
        }

        public Methods First()
        {
            use = 0;
            return select[use] as Methods;
        }
        public Methods Next()
        {
            use += move;
            if (!IsDone)
                return select[use] as Methods;
            else

                return null;
        }

        public int Step
        {
            get { return move; }
            set { move = value; }
        }

        public Methods CurrentMethods
        {
            get { return select[use] as Methods; }
        }

        public bool IsDone
        {
            get { return use >= select.Count; }
        }
    }
}
