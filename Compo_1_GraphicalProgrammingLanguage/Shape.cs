﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// Setter for shape's color and size
    /// </summary>
    /// <param name="colour"> Object of Shape's Color</param>
    abstract class Shape : Shapes //creating abstract shape class which extends shapes classs 
    {
        public Color colour; //shape's colour
        protected int x, y;

        /// <summary>
        /// creating shape constructor
        /// </summary>
        public Shape()
        {
            colour = Color.Red; //setting red color in shapes object
            x = y = 100;
        }

        /// <summary>
        /// Sets the colour and positin of the instance vairable by overloading the constructor.
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(Color colour, int x, int y)
        {
            this.colour = colour;
            this.x = x; //its x pos
            this.y = y; //its y pos
        }
        /// <summary>
        /// Calculate the area
        /// </summary>
        /// <returns></returns>
        public abstract double calcArea();
        public abstract double calcPerimeter();
        /// <summary>
        /// Creating draw method create a shape
        /// Seter for the colours, size and positino of shape
        /// </summary>
        /// <param name="g"> object of graphics to draw</param>
        /// <param name="fill"></param>
        public abstract void draw(Graphics g, bool fill);

        /// <summary>
        /// setter for size, position and color of shape
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="list"></param>
        public virtual void set(Color colour, params int[] list)
        {

            this.x = list[0];
            this.y = list[1];
            this.colour = colour;
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }

        public virtual void setColour(Color colour)
        {
            this.colour = colour;
        }

        /// <summary>
        /// Setting of the drawing starting point for the shape.
        /// </summary>
        /// <param name="list"></param>
        public virtual void setStartPoint(params int[] list)
        {
            this.x = list[0];
            this.y = list[1];
        }
    }
}
