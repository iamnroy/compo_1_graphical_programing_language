﻿using System;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Compo_1_GraphicalProgrammingLanguage
{
    public class Create
    {

        public delegate void ScreenRefreshDelegate();
        public delegate void TextDelegate(StringBuilder txt);
        public delegate void ExecuteDelegate(bool value);


        PictureBox display;
        Button runBtn;
        TextBox errorBox;

        public Graphics g;


        public SolidBrush Brush;

        public int xPos, yPos;

        public Color colour;


        public bool fill = false;

        public bool run = false;


        public Create(Graphics g, PictureBox display)
        {

            this.g = g;
            xPos = yPos = 175;
            colour = Color.Black;
            Brush = new SolidBrush(colour);
            this.display = display;
        }

        public void ScreenDelegate()
       {
        
                display.Invoke(new ScreenRefreshDelegate(RefereshDisplay));
     
        }

        public void DisplayDelegate(StringBuilder txt)
        {
            runBtn.Invoke(new TextDelegate(WriteErrors), txt);
        }

        public void EcecuteDelegate(bool val)
        {
            runBtn.Invoke(new ExecuteDelegate(BtnStatus), val);
        }

        public void RefereshDisplay()
        {
            display.Invalidate();
        }
        public void WriteErrors(StringBuilder errors)
        {
            this.errorBox.Text = errors.ToString();
        }
        public void BtnStatus(bool value)
        {
            runBtn.Enabled = value;
        }

        public void SetRunBtn(Button btn)
        {
            this.runBtn = btn;
        }
        public void SetTextBox(TextBox textbox)
        {
            this.errorBox = textbox;
        }

        public void SetBrushAndPenColor(String colorName)
        {
            this.colour = Color.FromName(colorName);
            Brush.Color = colour;

        }

        public void DrawLine(int x, int y)
        {

            g.DrawLine(new Pen(colour), xPos, yPos, x, y);
            xPos = x;
            yPos = y;



        }

        public void ChangeCursorLocation(int x, int y)
        {
            xPos = x;
            yPos = y;
        }


        public void Clear()
        {
            g.Clear(Color.Transparent);

        }

        public void Reset()
        {
            xPos = yPos = 175;
            fill = false;
            colour = Color.Black;
            g.Clear(Color.Transparent);
        }


        public void DrawCursor(Graphics g)
        {
            SolidBrush brush = new SolidBrush(Color.Black);


            g.Clear(Color.Transparent);
            g.FillEllipse(brush, xPos - 2, yPos - 2, 2 * 2, 2 * 2);
        }


    }
}
