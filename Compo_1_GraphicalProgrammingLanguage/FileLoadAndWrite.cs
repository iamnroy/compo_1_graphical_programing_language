﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// Creating fileloadandwrite class to save and load the file 
    /// </summary>
    class FileLoadAndWrite
    {
        ArrayList filecontent;
        string filePath; //variable to store the path of the file.

        /// <summary>
        /// This open the dialogbox to load file from system.
        /// </summary>
        /// <returns></returns>
        public ArrayList LoadFile()
        {
            using (OpenFileDialog openfileDialog = new OpenFileDialog())
            {


                filecontent = new ArrayList();

                openfileDialog.InitialDirectory = "D:\\Test_file";
                openfileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openfileDialog.FilterIndex = 2;
                openfileDialog.RestoreDirectory = true;

                if (openfileDialog.ShowDialog() == DialogResult.OK)
                {

                    filePath = openfileDialog.FileName;
                }
            }
            try
            {
                using (StreamReader s = File.OpenText(filePath))
                {
                    do
                    {

                        string line = s.ReadLine();
                        if (line == null) break;
                        filecontent.Add(line);

                    } while (true);

                }


            }
            catch (FileNotFoundException)
            {

            }
            catch (IOException ie)
            {

            }
            catch (Exception) { }
            return filecontent;
        }

        /// <summary>
        /// It opens the windows dialog box and write content into file.
        /// </summary>
        /// <param name="text"></param>
        public void WriteFile(String text)
        {
            using (SaveFileDialog savefileDialog = new SaveFileDialog())
            {

                savefileDialog.InitialDirectory = "D:\\Test_file";
                savefileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                savefileDialog.Title = "Save an Image File";
                savefileDialog.ShowDialog();

                if (savefileDialog.FileName != "")
                {
                    //optain specified file path
                    filePath = Path.GetFullPath(savefileDialog.FileName);
                    Console.WriteLine(filePath);
                    savefileDialog.Dispose(); //save the file in located path.
                }
            }

            try
            {

                using (StreamWriter s = File.CreateText(filePath))
                {
                    s.WriteLine(text);

                }

            }
            catch (IOException ie)
            {

            }
            catch (Exception i)
            {

            }
        }
    }
}
