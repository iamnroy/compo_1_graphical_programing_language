﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// Circle class extents the property of shape class
    /// </summary>
    class Circle : Shape
    {
        int radius;
        public Circle() : base()
        {
            radius = 100;
        }
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {

            this.radius = radius;
        }

        /// <summary>
        /// size and positin of circle,Setter for color.
        /// </summary>
        /// <param name="colour">Shapes's color</param>
        /// <param name="list">position and size of shape</param>
        public override void set(Color colour, params int[] list)
        {

            base.set(colour, list[0], list[1]);
            this.radius = list[2];

        }

        /// <summary>
        /// This draw the shape
        /// </summary>
        /// <param name="g">Graphics object use to draw</param>
        /// <param name="fill">shape filling or not</param>
        override
        public void draw(Graphics g, bool fill) //Creating graphics object to draw shape
        {
            //Creating pen's object 
            Pen p = new Pen(colour);
            SolidBrush b = new SolidBrush(colour);

            ///<summary> check condition of pen fill</summary>
            if (fill)
                g.FillEllipse(b, x - radius, y - radius, 2 * radius, 2 * radius);
            else
                g.DrawEllipse(p, x - radius, y - radius, 2 * radius, 2 * radius);
        }

        override
        public double calcArea()
        {
            return 3.14 * radius * radius;
        }

        override
        public double calcPerimeter()
        {
            return 2 * 3.14 * radius;
        }
    }
}
