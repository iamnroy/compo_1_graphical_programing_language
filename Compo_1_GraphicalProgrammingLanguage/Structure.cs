﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;


namespace Compo_1_GraphicalProgrammingLanguage
{
    public class Structure :Compile

    {
        Hashtable variable;
        public MethodCollection methods;
        Iterator iterator;
        DataTable dataTab;

        public Structure()
        {
            dataTab = new DataTable();
            variable = new Hashtable();
            methods = new MethodCollection();
        }
        public bool Syntax(string line, string word)
        {
            Match m = Regex.Match(line, @"\b" + word + @"\b");
            if (m.Success)
            {
                return true;
            }

            return false;

        }

        public bool SyntaxCheck(String text)
        {


            Console.WriteLine("****Inside VarSyntaxCheck****");
            int value;
            string[] tokens = Array.ConvertAll(text.ToLower().Trim().Split('='), s => s);
            tokens[0] = tokens[0].Trim();

            if (tokens.Length == 2)
            {


                //if variable is already contained overrride it
                if (variable.ContainsKey(tokens[0]) && int.TryParse(tokens[1], out value))
                {
                    int.Parse(tokens[1]);
                    Console.WriteLine(variable[tokens[0]]);

                    variable[tokens[0]] = value;

                    Console.WriteLine(variable[tokens[0]]);

                    return true;


                }
                //if varname exist but assigned value is not parsable
                else if (variable.ContainsKey(tokens[0]) && !int.TryParse(tokens[1], out value))
                {

                    //replacing all the variables found in second part of assignment
                    tokens[1] = VarToValue(tokens[1]);
                    Console.WriteLine("Token store after = sign parsed if var is found with result: " + tokens[1]);

                    if (int.TryParse(string.Format("{0}", dataTab.Compute(tokens[1], "")), out value))
                    {
                        variable[tokens[0]] = value;
                        Console.WriteLine(tokens[1] + " computed with return value " + value + " new value of variable is " + variable[tokens[0]].ToString());

                        return true;


                    }

                }
                //if variable is not contained  store it
                else
                {
                    Match m = Regex.Match(tokens[0], @"\b[a-zA-Z_]+[0-9]*\b");
                    int.Parse(tokens[1]);
                    if (m.Success && int.TryParse(tokens[1], out value))
                    {
                        variable.Add(tokens[0], value);
                        Console.WriteLine(tokens[0] + "added with value" + value);
                        return true;

                    }
                }

            }
            return false;

        }

        public string VarToValue(string line)
        {
            ICollection key = variable.Keys;

            Console.WriteLine(variable.Count);
            foreach (string k in key)
            {
                Console.WriteLine(k + "checking variables inside vartovalue");

                Match m = Regex.Match(line, @"\b" + k + @"\b");
                if (m.Success)
                {
                    Console.WriteLine(line + " ***this line is parsed for value***" + variable[k].ToString());
                    line = Regex.Replace(line, @"\b" + k + @"\b", variable[k].ToString());

                }

            }
            return line;

        }

        public bool ConditionCheck(string line)
        {
            /***
             * Needs to check condition not just by spliting with ' ' as conditions can have multiple ' 's
             */
            bool result = false;
            Console.WriteLine("checking condition for line: " + line);
            string[] splitLine = line.Split(' ');
            string condition = VarToValue(splitLine[1]);
            Console.WriteLine("*****" + condition + "******");

            DataTable dt = new DataTable();
            try
            {
                result = bool.Parse(string.Format("{0}", dt.Compute(condition, "")));
            }
            catch (Exception)
            {
                throw new InvalidCastException();
            }
            return result;
        }
        public Hashtable WhileIfSyntaxCheck(string[] cmd, string[] endpoint)
        {
            Console.WriteLine("****Inside WhileIfSyntaxCheck****");
            //tempoffset is kept one index higher then it should be because tempoffset is also used
            //as a flag for knowing that if is found if tempoffet > 0 so in case if is at the top tempoffset
            //whill still be set to zero and the algorithm thinks that if is not found.
            int tempoffset = 0; //store temp offset of if
            Hashtable arranged = new Hashtable(); //store arraged offset of all if's and endif's
            int count = 0; //count the number of if's after an if
            bool ifreturn = false;
            //get all if's and endif
            for (int j = 0; j < cmd.Length; j++)
            {

                //no if's store the offset of current if
                if (tempoffset == 0 && Syntax(cmd[j], endpoint[0]))
                {
                    //if the first if is found store it's index in temp
                    tempoffset = j + 1;
                    ifreturn = false;

                }
                else if (tempoffset != 0 && count == 0 && Syntax(cmd[j], endpoint[1]))
                {

                    arranged.Add(--tempoffset, j);
                    Console.WriteLine("Temp should be stored with if offset:" + tempoffset + "and endif offset " + j);
                    j = tempoffset;
                    tempoffset = 0;
                    count = 0;
                    ifreturn = true;

                }
                //if if is already found that means temoffset and findif should be true for condition below
                else if (tempoffset != 0)
                {
                    //if again another if found increase the number else decrease it

                    if (Syntax(cmd[j], endpoint[0])) { count++; Console.WriteLine("Count increased " + count); }
                    else if (Syntax(cmd[j], endpoint[1])) { count--; Console.WriteLine("Count increased " + count); }
                }

            }
            if (ifreturn)
                return arranged;
            else
                return null;
        }

        public bool ParseMethod(string line, Methods method)
        {

            string[] cmd = line.Split(' ');
            if (cmd.Length == 2)
            {
                string[] methodandparam = cmd[1].Split('(');
                if (methodandparam.Length == 2)
                {
                    method.methodName = methodandparam[0];
                    string[] param = methodandparam[1].Split(',');
                    param[param.Length - 1] = param[param.Length - 1].Replace(")", "");
                    method.param = param;
                    return true;
                }

            }
            return false;
        }

        public bool CheckForMethod(string line)
        {
            iterator = methods.BuildIterator();

            try
            {
                for (Methods method = iterator.First();
                !iterator.IsDone; method = iterator.Next())
                {
                    if (Syntax(line, method.methodName)) return true;
                }
            }
            catch (ArgumentOutOfRangeException) { }

            return false;
        }

        public string[] ParseMethodParameter(string cmd)
        {
            Console.WriteLine("****Inside ParseMethodParameter****");
            iterator = methods.BuildIterator();
            string[] methodandparam = cmd.Split('(');
            if (methodandparam.Length == 2)
            {
                Console.WriteLine(methodandparam[1] + " trying to split this with , ");
                string[] param = methodandparam[1].Split(',');
                //Console.WriteLine(param.Length + "**"+((Methods)methods[0]).param.Length + "**" + ((Methods)methods[0]).methodName);

                if (param.Length > 0)
                {
                    param[param.Length - 1] = param[param.Length - 1].Replace(")", "");


                    try
                    {
                        for (Methods method = iterator.First();
                        !iterator.IsDone; method = iterator.Next())
                        {
                            Console.WriteLine(method.methodName + "***" + methodandparam[0]);
                            if (method.methodName.Equals(methodandparam[0]) && method.param.Length == param.Length)
                            {
                                for (int i = 0; i < method.param.Length; i++)
                                {
                                    if (!method.param[i].Equals(""))
                                        variable.Add(method.param[i], param[i]);
                                }
                                return (string[])method.contents.ToArray(typeof(string));
                            }
                            else throw new ArgumentException();
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }



                }

            }
            return new string[0];
        }

        public void RemoveVar(string cmd)
        {
            iterator = methods.BuildIterator();
            string[] methodandparam = cmd.Split('(');
            if (methodandparam.Length == 2)
            {
                string[] param = methodandparam[1].Split(',');
                if (param.Length > 0)
                {
                    param[param.Length - 1] = param[param.Length - 1].Replace(")", "");

                    try
                    {
                        for (Methods method = iterator.First();
                        !iterator.IsDone; method = iterator.Next())
                        {
                            if (method.methodName.Equals(methodandparam[0]) && method.param.Length == param.Length)
                            {
                                for (int i = 0; i < method.param.Length; i++)
                                {
                                    variable.Remove(method.param[i]);

                                }

                            }
                        }
                    }
                    catch (ArgumentOutOfRangeException) { }

                }

            }

        }

    }
}
