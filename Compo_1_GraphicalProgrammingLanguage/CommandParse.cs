﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;


namespace Compo_1_GraphicalProgrammingLanguage
{
    public class CommandParse : Compile
    {


        Create cShape;

        ShapeFactory factory;

        Shape shape;
        int linenum = 0;
        public bool run = false;
        public bool checkcode = false;
        public bool end = false;
        Structure checkcmd;


        public CommandParse(Create cShape)
        {
            this.cShape = cShape;

            factory = new ShapeFactory();

        }

        public void DrawFromLine(String line, int linenum)
        {
            String[] split = line.Split(' '); //splitting the single line 

            if (split.Length == 2)
            {
                //split[1] =chcekcode.VarToValue(split[1]);

                //command and parameters from te command line 
                String Command = split[0];

                //getting shape 
                shape = factory.getShape(Command); //Shape shape = new Circle();

                //stores parameters or other command like colors and on/off for pen
                string[] parameters = split[1].Split(',');

                //checks if the command parsed as returned a shape or not
                if (shape != null)
                {
                    try
                    {

                        if (Command.Equals("circle") || Command.Equals("square"))
                        {
                            int radius = Int32.Parse(parameters[0]);
                            shape.set(cShape.colour, cShape.xPos, cShape.yPos, radius);
                        }
                        else if (parameters.Length == 2)
                        {
                            int param1 = Int32.Parse(parameters[0]);

                            int param2 = Int32.Parse(parameters[1]);

                            shape.set(cShape.colour, cShape.xPos, cShape.yPos, param1, param2);
                        }
                        else if (parameters.Length > 2)
                        {
                            if (parameters.Length % 2 != 0) throw new InvalidParameter();

                            int[] cordinates = new int[parameters.Length + 2];
                            cordinates[0] = cShape.xPos;
                            cordinates[1] = cShape.yPos;
                            int j = 0;
                            for (int i = 2; i < parameters.Length + 2; i++)
                            {
                                cordinates[i] =Int32.Parse( parameters[j]);
                                j++;
                            }
                            shape.set(cShape.colour, cordinates);

                        }


                        shape.draw(cShape.g, cShape.fill);

                    }
                    catch (FormatException)
                    {

                        throw new InvalidParameter();
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidParameter();
                    }

                }
                else if (Command.Equals("drawto") == true)
                {
                    Console.WriteLine(parameters[0] + " " + parameters[1]);
                    try
                    {
                        int param1 = Int32.Parse(parameters[0]);

                        int param2 = Int32.Parse(parameters[1]);

                        cShape.DrawLine(param1, param2);
                    }
                    catch (FormatException)
                    {
                        throw new InvalidParameter();

                    }

                }
                else if (Command.Equals("moveto") == true)
                {
                    try
                    {
                        int param1 = Int32.Parse(parameters[0]);

                        int param2 = Int32.Parse(parameters[1]);

                        cShape.ChangeCursorLocation(param1, param2);
                    }
                    catch (FormatException)
                    {
                        throw new InvalidParameter();

                    }
                }
                else if (Command.Equals("fill") == true)
                {
                    parameters[0] = parameters[0].ToLower().Trim();

                    if (parameters[0].Equals("on") == true)
                    {
                        cShape.fill = true;
                    }

                    else if (parameters[0].Equals("off") == true)
                    {
                        cShape.fill = false;
                    }

                    else throw new InvalidParameter();

                }
                else if (Command.Equals("pen") == true)
                {
                    parameters[0] = parameters[0].ToLower().Trim();

                    if (parameters[0].Equals("red")) { cShape.SetBrushAndPenColor("red"); }
                    else if (parameters[0].Equals("green")) { cShape.SetBrushAndPenColor("green"); }
                    else if (parameters[0].Equals("blue")) { cShape.SetBrushAndPenColor("blue"); }
                    else if (parameters[0].Equals("yellow")) { cShape.SetBrushAndPenColor("yellow"); }
                    else if (parameters[0].Equals("lime")) { cShape.SetBrushAndPenColor("lime"); }
                    else if (parameters[0].Equals("gold")) { cShape.SetBrushAndPenColor("gold"); }
                    else if (parameters[0].Equals("cyan")) { cShape.SetBrushAndPenColor("cyan"); }
                    else if (parameters[0].Equals("tomato")) { cShape.SetBrushAndPenColor("tomato"); }
                    else if (parameters[0].Equals("black")) { cShape.SetBrushAndPenColor("black"); }
                    else throw new InvalidParameter();
                }
                else
                {
                    //different error display for different textbox inputs.
                    if (linenum != 0)
                    {
                        errors.Add("Invalid command and parameter given at line: " + linenum + "\r\n");
                    }

                    else
                        errors.Add("Invalid command and parameter given." + "\r\n");
                }

            }

            else if (split.Length == 1)
            {

                if (line.Equals("clear") == true)
                {
                    cShape.Clear();

                }
                else if (line.Equals("run") == true)
                {
                    if (!run) cShape.run = true;
                }
                else if (line.Equals("reset") == true)
                {
                    cShape.Reset();

                }
                else
                {
                    throw new InvalidCommand("Invalid command");
                }
                //block which handles one line command ends
            }

            else if (split.Length > 2)
            {

                throw new InvalidCommand("Invalid command");
            }

            if (linenum != 0) cShape.ScreenDelegate();

        }

        public void DrawFromText(String text)
        {
            String line = text.ToLower().Trim();
            string[] cmd = line.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            SyntaxParser(cmd);

        }

        public int[] StringToInt(String[] str)
        {
            return Array.ConvertAll(str, s => int.Parse(s));
        }

        public void SyntaxParser(String[] cmd)
        {
            cShape.EcecuteDelegate(false);
            StringBuilder picText = new StringBuilder();
            run= true;
            bool[] isloop = new bool[100];
            int[] loopindex = new int[100];
            bool methodLookahead = false;
            Methods method = new Methods();
            int count = -1;

            Hashtable ifendifoffset = checkcmd.WhileIfSyntaxCheck(cmd, new string[] { "if", "endif" });
            Hashtable whileendloopoffset = checkcmd.WhileIfSyntaxCheck(cmd, new string[] { "while", "endloop" });
            Hashtable methodloopoffset = checkcmd.WhileIfSyntaxCheck(cmd, new string[] { "method", "endmethod" });


            /*  for (int i = 0; i < cmd.Length; i++)
              {
                  if (end) { end = false;
                      break; 
                  } //break the loop in code

                  this.linenum = i;
                  linenum++;
                  try
                  {
                      DrawFromLine(cmd[i], linenum);

                  }
                  catch (InvalidCommand a)
                  {

                      errors.Add("Invalid command at line : " + linenum + "\r\n");
                      Console.WriteLine(a);
                  }
                  catch (InvalidParameter)
                  {
                      errors.Add("Parameter given for the command is invalid at: " + linenum + "\r\n");

                  }
                  catch (IndexOutOfRangeException e)
                  {
                      Console.WriteLine(e);
                  }
             }*/
            


            try
            {

                for (int i = 0; i < cmd.Length; i++)
                {
                    if (end) { end = false; break; } //break the loop 
                    this.linenum = i;
                    linenum++;
                    //first check if the line contains variable declaration or reassignment
                    if (cmd[i].Contains("=") || checkcmd.CheckForMethod(cmd[i]))
                    {
                        //don't forget to check method param length eual args length or not
                        if (checkcmd.CheckForMethod(cmd[i]) && !methodLookahead)
                        {
                            string[] newcmd = checkcmd.ParseMethodParameter(cmd[i]);
                            //Console.WriteLine(((Methods)checkcmd.methods[0]).methodName);
                            if (newcmd.Length > 0)
                            {
                                SyntaxParser(newcmd);
                               // Console.WriteLine("var count before " + checkcmd.variables.Count);
                                checkcmd.RemoveVar(cmd[i]);

                                //Console.WriteLine("var count after " + checkcmd.variables.Count);
                            }
                            else Console.WriteLine("Invalid Method Name or Parameter given");

                        }

                        if (cmd[i].Contains("=")) checkcmd.SyntaxCheck(cmd[i]);


                    }
                    //if the line contains if and endif statement
                    else if (checkcmd.Syntax(cmd[i], "if") || checkcmd.Syntax(cmd[i], "endif"))
                    {
                        try
                        {
                            if (ifendifoffset == null)
                            {
                                errors.Add("Invalid if syntax without proper ending!");
                                break;
                            }
                            //if line contains if
                            else if (checkcmd.Syntax(cmd[i], "if"))
                            {

                                //if condition is false
                                if (!checkcmd.ConditionCheck(cmd[i]))
                                {
                                    //if the statement has proper ending
                                    for (int j = i; j < cmd.Length; j++)
                                    {
                                        //if has proper ending go to the index of endif
                                        if (checkcmd.Syntax(cmd[j], "endif") && ifendifoffset.Contains(i) && (((int)ifendifoffset[i]) == j))
                                        {

                                            i = j;
                                        }
                                    }
                                }
                                //if condition is true keep the index value of endif for other if's
                                else if (checkcmd.ConditionCheck(cmd[i]))
                                {
                                    //do nothing for now.
                                }
                            }


                        }
                        catch (IndexOutOfRangeException)
                        {

                        }



                    }
                    //if syntax checking ends
                    else if (checkcmd.Syntax(cmd[i], "while") || checkcmd.Syntax(cmd[i], "endloop"))
                    {
                        if (whileendloopoffset == null)
                        {
                            errors.Add("Invalid while syntax without proper ending!");
                            break;
                        }
                        //check for while keyword
                        else if (checkcmd.Syntax(cmd[i], "while"))
                        {

                            //for condition true
                            if (checkcmd.ConditionCheck(cmd[i]))
                            {
                                //increase the whilecount to make it a unique index
                                //set isloop true and store loopindex to the unique index
                                count++;
                                isloop[count] = true;
                                loopindex[count] = i;
                            }
                            //for false while condition
                            else if (!checkcmd.ConditionCheck(cmd[i]))
                            {
                                //find the endloop keyword and if it's assosiated with this while move to that index.
                                for (int j = i; j < cmd.Length; j++)
                                {
                                    if (checkcmd.Syntax(cmd[j], "endloop") && whileendloopoffset.Contains(i) && (((int)whileendloopoffset[i]) == j))
                                    {
                                        i = j;
                                    }

                                }
                            }

                        }
                        //now if endloop is found and condition is false for this specific while block
                        //change isloop to false for this while block and decrease the whilecount to match with parent loop
                        //else condition is true for this block move to that index.
                        else if (checkcmd.Syntax(cmd[i], "endloop"))
                        {
                            try
                            {
                                if (!checkcmd.ConditionCheck(cmd[loopindex[count]]))
                                {
                                    isloop[count] = false;
                                    count--;
                                }
                                else i = loopindex[count];
                            }
                            catch (IndexOutOfRangeException e)
                            {
                                errors.Add("Index out of bound Exception!");
                                break;
                            }



                        }

                    }
                    else if (checkcmd.Syntax(cmd[i], "method") || methodLookahead || checkcmd.Syntax(cmd[i], "endmethod"))
                    {


                        if (checkcmd.Syntax(cmd[i], "method") && !methodLookahead)
                        {
                            if (methodloopoffset == null)
                            {
                                errors.Add("Method declaration doesn't have ending : " + linenum);
                                break;
                            }


                            //ParseMethod stores new method
                            if (checkcmd.ParseMethod(cmd[i], method))
                            {
                                methodLookahead = true;
                            }
                            else
                            {
                                errors.Add("Invalid method declaration at line : " + linenum);
                                break;
                            }
                        }
                        else if (checkcmd.Syntax(cmd[i], "method") && methodLookahead)
                        {
                            errors.Add("Can't declare a method inside another!");
                            break;
                        }

                        else if (methodLookahead && checkcmd.Syntax(cmd[i], "endmethod"))
                        {
                            methodLookahead = false;
                            Methods tempmethod = new Methods();
                            tempmethod.methodName = method.methodName;
                            tempmethod.param = method.param;
                            foreach (string s in method.contents)
                            {
                                tempmethod.ContentAdding(s);
                            }
                            checkcmd.methods.Add(tempmethod);

                            //also call a method here that stores all the line,methodname and params
                        }
                        else if (methodLookahead)
                        {
                            //store current line in something.

                            if (checkcmd.Syntax(cmd[i], method.methodName))
                            {
                                errors.Add("Method can't be nested!");
                                break;
                            }
                            method.ContentAdding(cmd[i]);
                        }

                    }
                    else
                    {
                        try
                        {
                            DrawFromLine(cmd[i], linenum);

                        }
                        catch (InvalidCommand a)
                        {

                            errors.Add("Invalid command at line : " + linenum + "\r\n");
                            Console.WriteLine(a);
                            cShape.ScreenDelegate();
                        }
                        catch (InvalidParameter)
                        {
                            errors.Add("Parameter given for the command is invalid at: " + linenum + "\r\n");
                            cShape.ScreenDelegate();
                        }
                        catch (IndexOutOfRangeException e)
                        {
                            Console.WriteLine(e);
                            cShape.ScreenDelegate();
                        }



                    }
                    //main loop ends here


                    Thread.Sleep(1);
                }


            }
            catch (InvalidCastException)
            {
                errors.Add("Invalid condition given for if or while syntax!");
            }
            catch (InvalidCommand)
            {
                errors.Add("Invalid syntax at line: " + linenum);
            }
            catch (FormatException)
            {
                errors.Add("Invalid value Assigned to the variable at line: " + linenum + "\r\n");
            }
            catch (EvaluateException)
            {
                errors.Add("Invalid value reassigned at line: " + linenum + "\r\n");
            }
            catch (ArgumentException)
            {
                errors.Add("Arguments given don't match parameters for the method at line : " + linenum);
            }


            /*
             * Process to be move to thread end because it should be executed at the end only once but this method runs multiple times.
             */

            //Get all the errors from parser and display it on console
            if (errors.Count != 0)
            {
                foreach (var item in errors)
                    picText.Append(item.ToString());
            }
            else if (errors.Count == 0 && checkcode) picText.Append("No errors found!");
            cShape.DisplayDelegate(picText);

            //after computing everyting draw and set chcekcode to false
            run = false;
            ClearErrors();
            if (checkcode) cShape.Reset(); //reset the everything if syntax check is on
            checkcode = false;
            end = false;
            cShape.EcecuteDelegate(true);//enable run btn at the end of the thread.

            Console.WriteLine("Check syntax value changed to : " + checkcode);


        }

    }
}
