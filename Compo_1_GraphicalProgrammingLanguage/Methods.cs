﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace Compo_1_GraphicalProgrammingLanguage
{
    public class Methods
    {
        public string methodName
        { get; 
          set; 
        }

        public string[] param 
        { get; 
          set; 
        }

        public ArrayList contents;

        public Methods()
        {
            contents = new ArrayList();
        }

        public void ContentAdding(string line)
        {
            contents.Add(line);
        }

        public void Clear()
        {
            methodName = "";
            param = null;
            contents.Clear();
        }

    }
}
