﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
//using System.ComponentModel;
//using System.Collections.Generic;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public partial class ASE : Form
                
    {
        /// <summary>
        /// 
        /// </summary>
        readonly Bitmap OutputBitmap = new Bitmap(1008, 707);
        readonly Bitmap temp_screen = new Bitmap(1008, 707);
        readonly Create createShape;
        readonly CommandParse parser;
        ArrayList filecontent;
        public ASE()
        {
            InitializeComponent();
            createShape = new Create(Graphics.FromImage(OutputBitmap), displayWindow);
            parser = new CommandParse(createShape);
            createShape.DrawCursor(Graphics.FromImage(temp_screen));
            createShape.SetRunBtn(btn_run);
            createShape.SetTextBox(console);

        }



        private void ASE_Load(object sender, EventArgs e)
        {

        }

        private void cmdLine_Box(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !cmd.Text.Equals(""))
            {
                console.Text = "";

               
                    string SingleLine = cmd.Text;
                    SingleLine = SingleLine.ToLower();

                    if (SingleLine.Equals("run"))
                    {

                        btn_run.PerformClick();
                        
                    }
                    else if (SingleLine.Equals("clear"))
                    {
                        createShape.Clear();
                        Refresh();
                    }
                    else if (SingleLine.Equals("reset"))
                    {
                        createShape.Reset();
                        Refresh();
                    }
                    else
                    {
                        console.Text = "Inavlid Command";
                    }
               

                if (parser.getErrorCount() != 0)
                {
                    foreach (var item in parser.errors)
                        console.Text += item.ToString();
                }
                Refresh();

                parser.ClearErrors();
            }
        }

        private void btnExecute(object sender, EventArgs e)
        {
            string SingleLine = cmd.Text;
            SingleLine = SingleLine.ToLower();

            Console.Write(SingleLine+"\n");
            if (SingleLine.Equals("run"))
            {
                console.Text = "";

                string cmd = cmdText.Text;
                Console.Write(cmd);
                parser.DrawFromText(cmd);

            }

            else if (SingleLine.Equals("clear"))
            {
                createShape.Clear();
                Refresh();
            }
            else if (SingleLine.Equals("reset"))
            {
                createShape.Reset();
                Refresh();
            }
            else
            {
                console.Text = "Inavlid Command";
            }

            if (parser.errors.Count != 0)
            {
                foreach (var item in parser.errors)
                    console.Text += item.ToString();
            }
            Refresh();

            parser.ClearErrors();

        }

        private void displayBox(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            createShape.DrawCursor(Graphics.FromImage(temp_screen));


            g.DrawImageUnscaled(OutputBitmap, 0, 0);
            g.DrawImageUnscaled(temp_screen, 0, 0);
            temp_screen.MakeTransparent();
        }

        private void file_load_Click(object sender, EventArgs e)
        {
            cmdText.Text = "";
            filecontent = new FileLoadAndWrite().LoadFile();

            foreach (string s in filecontent)
            {
                cmdText.Text = s+"\n"+ cmdText.Text;
            }
        }

        private void SaveTool(object sender, EventArgs e)
        {
            new FileLoadAndWrite().WriteFile(cmdText.Text);

        }

        private void cmdText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
