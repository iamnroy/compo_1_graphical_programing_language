﻿using System;


namespace Compo_1_GraphicalProgrammingLanguage
{

    /// <summary>
    /// Crating shapefactory class to call the class objects for draw.
    /// </summary>
    class ShapeFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shapeType"></param>
        /// <returns></returns>

        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();


            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle(); //calling circle class

            }
            if (shapeType.Equals("RECT"))
            {
                return new Rectangle(); //calling rectangle class

            }

            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle(); //calling triangle class
            }

            else
            {
                return null;

            }


        }

    }
}
