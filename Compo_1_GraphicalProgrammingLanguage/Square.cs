﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    class Square : Shape
    {
        int width;

        public Square() : base()
        {
            width = 100;
        }

        public Square(Color colour, int x, int y, int width)
        {
            this.width = width;
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.width = list[2];
        }

        public override void draw(Graphics g, bool fill)
        {
            Pen p = new Pen(colour);
            SolidBrush b = new SolidBrush(colour);

             g.FillRectangle(b, (x - width / 2), (y - width / 2), width, width);
             g.DrawRectangle(p, (x - width / 2), (y - width / 2), width, width);
        }
    }
}
