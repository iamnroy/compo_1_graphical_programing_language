﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public class MethodCollection
    {
        private ArrayList method = new ArrayList();

        public Iterator BuildIterator()
        {
            return new Iterator(this);
        }

        public int Count
        {
            get
            {
                return method.Count;
            }

        }
        public void Clear()
        {
            method.Clear();
        }

        public object this[int index]
        {
            get { return method[index]; }
            set { method.Add(value); }
        }

        public void Add(Methods value)
        {
            method.Add(value);
        }

    }
}
