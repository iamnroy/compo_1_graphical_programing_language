﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public class InvalidParameter : System.Exception
    {
        public InvalidParameter() : base("Invalid Parameter given for the command") { }
        public InvalidParameter(string message) : base(message) { }
        public InvalidParameter(string message, System.Exception inner) : base(message, inner) { }

        protected InvalidParameter(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
