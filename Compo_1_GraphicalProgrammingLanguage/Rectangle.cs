﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// creating rectangle class for creating its shape
    /// </summary>
    class Rectangle : Shape
    {
        /// <summary>
        /// creating int variable witdh and height to store rectangle balue
        /// </summary>
        int width, height;

        /// <summary>
        ///  Rectangle constructor for initializing its size.
        /// </summary>
        public Rectangle() : base()
        {
            Console.WriteLine("rect obj created");
            width = 100;
            height = 100;
        }
        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="colour"> Color of Rectangle</param>
        /// <param name="x"> </param>
        /// <param name="y"></param>
        /// <param name="width">Width of rectangle</param>
        /// <param name="height">height of rectangle</param>
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Setter for the rectangle color, shape and its position
        /// </summary>
        /// <param name="colour"> Color of rectangle</param>
        /// <param name="list"></param>
        public override void set(Color colour, params int[] list)
        {


            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"> Object of graphic for create an shape</param>
        /// <param name="fill"></param>
        override
        public void draw(Graphics g, bool fill)
        {
            //Creating object of pen for drawing a line
            Pen p = new Pen(colour);
            SolidBrush b = new SolidBrush(colour);

            Console.WriteLine("I drew a rectangle");

            if (fill)
                g.FillRectangle(b, (x - width / 2), (y - height / 2), width, height);
            else
                g.DrawRectangle(p, (x - width / 2), (y - height / 2), width, height);

            Console.WriteLine("this is the base colour" + base.colour);
        }

        override
        public double calcArea()
        {
            return width * height;
        }

        override
        public double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }
}
