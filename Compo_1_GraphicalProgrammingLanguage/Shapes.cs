﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    /// <summary>
    /// Creating interface Shape class for creating all the shapes.
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// Setter of sha
        /// </summary>
        /// <param name="list">position and size of the shape</param>
        void setStartPoint(params int[] list);

        void setColour(Color colour);

        void draw(Graphics g, bool fill);
        double calcArea();
        double calcPerimeter();
    }
}
