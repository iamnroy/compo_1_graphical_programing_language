﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compo_1_GraphicalProgrammingLanguage
{
    public class InvalidCommand : Exception
    {
        public InvalidCommand() : base("Invalid command given.") { }
        public InvalidCommand(string message) : base(message) { }
        public InvalidCommand(string message, System.Exception inner) : base(message, inner) { }

        protected InvalidCommand(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
